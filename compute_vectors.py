import numpy as np
import surfaces
import os
from tqdm import tqdm
paths = {"SHREC":"D:\\these_datasets\\SHREC14\\Real\\Data", "FAUST":"D:\\these_datasets\\FAUST\\MPI-FAUST\\training\\scans"}

os.makedirs("result", exist_ok=True)
N = 1000
seed = 4145546542
np.random.seed(seed)
meta_vec = {}
vec_sime = np.array(([[1,0,0], [0,1,0], [0,0,1]]))
print(vec_sime.shape)
meta_vec["simple"] = vec_sime
for i in range(10):
    N = 2**(i+2)
    vectors = np.random.rand(N,3)
    vectors = vectors/np.linalg.norm(vectors, axis=1, keepdims=True)
    sqrt = int(np.sqrt(N))
    phi = (np.pi/2)*np.linspace(0,1,sqrt)
    teta = (np.pi/2)*np.linspace(0,1,sqrt)
    t = np.ones(sqrt)
    vectors_abs = np.array([np.outer(np.sin(teta), np.cos(phi)).ravel(), np.outer(np.sin(teta), np.sin(phi)).ravel(),
                            np.outer(np.cos(teta), t).ravel()]).T
    meta_vec["rand_{0}".format(N)] = vectors
    meta_vec["equi_{0}".format(N)] = vectors_abs
for key in paths:
    files = [f for f in os.listdir(paths[key]) if ".obj" in f or ".ply" in f]
    for vecs in meta_vec:
        vectors = meta_vec[vecs]
        N = vectors.shape[0]
        result = np.zeros((len(files), N))
        j = 0
        for file in tqdm(files, "Processing {0}".format(key)):
            surface = surfaces.Surface(surf=None, filename=os.path.join(paths[key], file), FV = None)
            X_N = surfaces.varifoldJC(surface)
            norm = np.linalg.norm(X_N, axis=1, keepdims=True)
            X_N = X_N/norm
            for l in range(N):
                u = vectors[l, :]
                result[j, l] = (np.absolute(surface.surfel @ u)).sum()
            j+=1
        np.save(os.path.join("result","result_{0}_{1}.npy".format(key, vecs)), result)