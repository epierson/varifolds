import numpy as np
import surfaces
import kernelFunctions as kfun
import os
import time
from tqdm import tqdm
typeKernel = 'gauss'
typeKernel = 'laplacian'
paths = {"SHREC":"D:\\these_datasets\\SHREC14\\Real\\Data", "FAUST":"D:\\these_datasets\\FAUST\\MPI-FAUST\\training\\scans"}


#choisir un type de noyau: gaussien ou quelque chose d'un peut moins régulier
sigmaKernel = 1.5
KparDist = kfun.Kernel(name=typeKernel, sigma=sigmaKernel)
tet1 = [[(0,1,3), (1,2,3), (0,2,3), (0,1,2)],
        [(0,0,0), (1,0,0), (1,0,1), (1,1,0)]]
#FirstSurface = surfaces.Surface(surf=None, filename=os.path.join(big_path, files[0]), FV = None)
#FirstSurface = surfaces.Surface(surf=None, filename=None, FV=tet1)
# Crée un objet de classe surface pour la première surface. 
# Mettre dans FV une liste [F,V] avec F une liste des indices des faces, i.e. 
# F=[F1 F2 F3...] avec F1=[i j k] les trois indices correspondant des points de la première face, et V = [x1 x2 x3...]
# les sommets de la triangulation, avec x1 par exemple les 3 coordonnées du premier sommet.
# On peut à la place mettre dans surf un autreobjet de classe surface, 
# OU dans filename un fichier surface de format byu, off, vtk ou obj.

#SecondSurface = surfaces.Surface(surf=None, filename=os.path.join(big_path, files[1]), FV = None)
# Crée un objet de classe surface pour la seconde surface. 
t1 = time.time()
#print(surfaces.varifoldNorm(FirstSurface, SecondSurface, KparDist))
# Calcule le carré de la distance varifold entre deux surfaces.
t2 = time.time()
#print(surfaces.currentNorm(FirstSurface, SecondSurface, KparDist))
# Calcule le carré de la distance courant entre deux surfaces.
t3 = time.time()
print("Varifold time:", t2-t1)
print("Current time:", t3-t2)




# également calculable: gradient de ces normes en fonction des sommets de FirstSurface avec
# surfaces.varifoldNormGradient(FirstSurface, SecondSurface, KparDist) et 
# surfaces.currentNormGradient(FirstSurface, SecondSurface, KparDist)